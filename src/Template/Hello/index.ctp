<table>
<?= $this->Html->tableHeaders(['title', 'name', 'mail'],
                              ['style' => ['background:#006;color:white']]) ?>
<?= $this->Html->tableCells([
        ['this is sample', 'taro', 'taro@yamada'],
        ['Hello!', 'hanako', 'hanako@flower'],
        ['test,test.', 'sachiko', 'sachiko@co.jp'],
        ['last!.', 'jiro', 'jiro@change'],
    ],
    ['style' => ['background:#ccf']],
    ['style' => ['background:#dff']]
    ) ?>
</table>

<ul>
<?= $this->Html->nestedLIst(
        ['first line', 'second line', 'third line' => ['one', 'two', 'three']]
) ?>
</ul>

<?= $this->Url->build(['controller' => 'hello', 'action' => 'show', '123']) ?>
<br>
<?= $this->Url->build(['controller' => 'hello', 'action' => 'show',
    '?' => ['id' => 'taro', 'password' => 'yamada123']]) ?>
<br>
<?= $this->Url->build(['controller' => 'hello', 'action' => 'show',
    '_ext' => 'png', 'sample']) ?>
<br>
<?= $this->Text->autoLinkUrls('http://google.com') ?>
<br>
<?= $this->Text->autoLinkEmails('syoda@tuyano.com') ?>