<?php if (!empty($bidinfo)) : ?>
    <h2>商品「<?= $bidinfo->biditem->name ?>」</h2>
    <h3>※今回の取引について出品者を評価する</h3>
    <h6>※評価項目の入力</h6>
    <?= $this->Form->create($bidEval) ?>
    <?= $this->Form->hidden('bidinfo_id', ['value' => $bidinfo->id]) ?>
    <?= $this->Form->hidden('user_id', ['value' => $authuser['id']]) ?>
    <?= $this->Form->select('stars', [1=>1,2=>2,3=>3,4=>4,5=>5]) ?>
    <?= $this->Form->textarea('comment', ['rows' => 2]); ?>
    <?= $this->Form->button('Submit') ?>
    <?= $this->Form->end() ?>
<?php else: ?>
    <h2>※落札情報はありません。</h2>
<?php endif; ?>

<h3>※あなたが評価した一覧</h3>
<table cellpadding="0" cellspacing="0">
    <thead>
        <!-- <th class="main" scope="col"><?= $this->Paginator->sort('bidinfo_id') ?></th> -->
        <!-- <th scope="col"><?= $this->Paginator->sort('user_id') ?></th> -->
        <th class="main" scope="col"><?= $this->Paginator->sort('name') ?></th>
        <th class="main" scope="col"><?= $this->Paginator->sort('username') ?></th>
        <th scope="col"><?= $this->Paginator->sort('stars') ?></th>
        <th scope="col"><?= $this->Paginator->sort('comment') ?></th>
        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
        <!-- <th scope="col" class="actions"><?= __('Actions') ?></th> -->
    </thead>
    <tbody>
        <?php if (!empty($bidevaluations)) : ?>
            <?php foreach ($bidevaluations as $bideval) : ?>
            <tr>
                <!-- <td><?= h($bideval->bidinfo_id) ?></td>
                <td><?= h($bideval->user_id) ?></td> -->
                <td><?= h($bideval->bidinfo->biditem->name) ?></td>
                <td><?= h($bideval->bidinfo->biditem->user->username) ?></td>
                <td><?= h($bideval->stars) ?></td>
                <td><?= h($bideval->comment) ?></td>
                <td><?= h($bideval->created) ?></td>
                <td></td>
                <!-- <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $biditem->id]) ?>
                </td>
                -->
            </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr><td colspan="3">※まだ評価はありません。</td></tr>
        <?php endif; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< '.__('first')) ?>
        <?= $this->Paginator->prev('< '.__('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next').' >') ?>
        <?= $this->Paginator->last(__('last').' >>') ?>
    </ul>
</div>