<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?><!-- <meta charset="utf-8"/> -->
    <!-- コントローラ名を取り出す -->
    <title><?= $this->fetch('title') ?></title>
    <?= $this->Html->css('hello') ?><!-- <link rel="stylesheet" href="/mycakeapp3/css/hello.css"/> -->
    <?= $this->Html->script('hello') ?><!-- <script src="/mycakeapp3/js/hello.js"></script> -->
</head>

<body>
    <header class="head row">
        <!-- エレメントのheader.ctpを読みj込み subtitleを渡す -->
        <?= $this->element('header', $header) ?>
    </header>
    <div class="content row">
        <!-- index.ctpの内容 -->
        <?= $this->fetch('content') ?>
    </div>
    <footer class="foot row">
        <!-- エレメントのfooter.ctpを読みj込み copyrightを渡す -->
        <?= $this->element('footer', $footer) ?>
    </footer>
</body>
</html>