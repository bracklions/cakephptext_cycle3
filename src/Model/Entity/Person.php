<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Person extends Entity {
    
    // 3-7
    protected $_accessible = [
        'name' => true,
        'mail' => true,
        'age' => true
    ];
}