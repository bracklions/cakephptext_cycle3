<?php
// ここで定義するクラスがどこに配置されるか
namespace App\Controller;

// いちいちApp\Controller\HelloControllerと書かなくてもいいように
use App\Controller\AppController;

class HelloController extends AppController {
    // 自動レンダリングをoff　テンプレート読み込まないように
    // public $autoRender = false;
    
    public function initialize() {
        // hello.ctpを使用する
        $this->viewBuilder()->setLayout('hello');
    }

    public function index() {
        // 2-26
        $this->set('header', ['subtitle' => 'from Controller with Love♡']);
        $this->set('footer', ['copyright' => '名無しの権兵衛']);
        // 2-7
        // $this->viewBuilder()->autoLayout(false);
        //2-11
        // $this->set('title', 'Hello!');

        // 2-14
        // if ($this->request->isPost()) {
        //     $this->set('data', $this->request->data['Form1']);
        // } else {
        //     $this->set('data', []);
        // }
        // 2-8
        // $values = [
        //     'title' => 'Hello!',
        //     'message' => 'This is message!'
        // ];
        // $this->set($values);
        // 2-7
        // $this->set('title', 'Hello!');
        // $this->set('message', 'This is message!');
        // $id = 'no name';
        // if (isset($this->request->query['id'])) {
            // クエリパラメータ(アドレスに付け足して送るパラメータ)を使う idかpassどっちかだけでも渡せる
            // $id = $this->request->query['id'];
        // }
        // $pass = 'no password';
        // if (isset($this->request->query['pass'])) {
        //     $pass = $this->request->query['pass'];
        // }
        // echo "<html><body><h1>Hello</h1>";
        // echo "<ul><li>your id: ".$id."</li>";
        // echo "<li>password: ".$pass."</li></ul>";
        // echo "</body></html>";
    }

    public function form() {
        // 2-11
        $this->viewBuilder()->autoLayout(false);
        // formからの値がdataというプロパティに入っている
        $name = $this->request->data['name'];
        $mail = $this->request->data['mail'];
        $age = $this->request->data['age'];
        $res = 'こんにちは'.$name.')'.$age.')さん。メールアドレスは、'.$mail.'ですね？';
        $values = [
            'title' => 'Result',
            'message' => $res
        ];
        $this->set($values);
    }
}