<?php
// ここで定義するクラスがどこに配置されるか
namespace App\Controller;

// いちいちApp\Controller\HelloControllerと書かなくてもいいように
use App\Controller\AppController;

class PeopleController extends AppController {

        public $paginate = [
            'finder' => 'byAge',
            'limit' => 5,
            'contain' => ['Messages'],
        ];

        public function initialize() {
            parent::initialize();
            $this->loadComponent('Paginator');
        }

        public function index() {
            // 5-18
            $data = $this->paginate($this->People);
            // 4-2
            // if ($this->request->is('post')) {
                // $find = $this->request->data['People']['find']; // $this->Form->text('People.find') で作られた<input type="text" name="People[find]"/>から取得
                // $condition = ['conditions' => ['name' => $find]]; // nameの値が$findかどうかチェック
                // 4-3
                // $condition = ['conditions' => ['name like' => $find]];
                // $data = $this->People->find('all', $condition);
                // 4-9 4-10
                // $data = $this->People->findByNameOrMail($find, $find);// findBy + Name or Mail
                // 4-11
                // $data = $this->People->find()->where(['name' => $find]);
                // 4-12
                // $data = $this->People->find()->where(['name like' => $find]);
                // 4-13 4-14 4-15
                // $arr = explode(',', $find);
                // $connection = \Cake\Datasource\ConnectionManager::get('default'); // DB接続を取得
                // $data = $this->People->find()
                //     ->order(['People.age' => 'asc'])
                //     ->order(['People.name' => 'asc'])
                    // ->where(['age >=' => $arr[0]])
                    // ->andwhere(['age <=' => $arr[1]])
                    // ->order(['People.age' => 'asc'])
                    // ->order(['People.name' => 'asc'])
                    // ->limit(3)->page($find);
                // 4-17
                // $data = $this->People->find('me', ['me' => $find])
                //     ->contain(['Messages']);
            // } else {
                // 3-14
                // $data = $this->People->find('all');
                // 4-9
                // $data = $this->People->find('all',
                //     ['order' => ['People.age' => 'asc']]
                // );
                // 4-14
                // $data = $this->People->find()
                //     ->order(['People.age' => 'asc'])
                //     ->order(['People.name' => 'asc']);
                // 4-17
                // $data = $this->People->find('byAge')
                //     ->contain(['Messages']);
            // }
            // 3-12
            // $id = $this->request->query['id'];
            // $data = $this->People->get($id);
            // $data = $this->People->find('list');
            $this->set('data', $data);
        }

        public function add() {
            $msg = 'please type your personal data...';
            $entity = $this->People->newEntity();
            if ($this->request->is('post')) {
                $data = $this->request->data['People'];
                $entity = $this->People->newEntity($data);
                if ($this->People->save($entity)) {
                    return $this->redirect(['action' => 'index']);
                }
                $msg = 'Error was occured...';
            }
            $this->set('msg', $msg);
            $this->set('entity', $entity);
        }

        public function create() {
            if ($this->request->is('post')) {
                // inputタグがみんなPeople.〜だから、セットされる
                $data = $this->request->data['People'];
                // テーブルクラス名->newEntityで新しいエンティティを作成
                $entity = $this->People->newEntity($data);
                $this->People->save($entity);
            }
            return $this->redirect(['action' => 'index']);
        }

        public function edit() {
            $id = $this->request->query['id'];
            $entity = $this->People->get($id);
            $this->set('entity', $entity);
        }

        public function update() {
            if ($this->request->is('post')) {
                $data = $this->request->data['People'];
                $entity = $this->People->get($data['id']);
                // 第２引数->第１引数で更新
                $this->People->patchEntity($entity, $data);
                $this->People->save($entity);
            }
            return $this->redirect(['action' => 'index']);
        }

        public function delete() {
            $id = $this->request->query['id'];
            $entity = $this->People->get($id);
            $this->set('entity', $entity);
        }

        public function destroy() {
            if ($this->request->is('post')) {
                $data = $this->request->data['People'];
                $entity = $this->People->get($data['id']);
                $this->People->delete($entity);
            }
            return $this->redirect(['action' => 'index']);
        }
}