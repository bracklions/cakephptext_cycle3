<?php
// ここで定義するクラスがどこに配置されるか
namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;

class AuctionBaseController extends AppController {
    
    public function initialize() {
        parent::initialize();
        // 必要なコンポーネントのロード
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'],
            'authenticate' => [                     // 認証で使うフォームの内容など
                'Form' => [
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'logout',
            ],
            'authError' => 'ログインして下さい。',
        ]);
    }

    // ログイン処理
    public function login() {
        // POST時の処理
        if ($this->request->isPost()) {
            // ログインしたユーザーのEntityを取り出す
            $user = $this->Auth->identify();
            // Authのidentifyをユーザーに設定
            if (!empty($user)) {
                $this->Auth->setUser($user);
                // 元のページに戻る
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('ユーザー名かパスワードが間違っています。');
        }
    }

    // ログアウト処理
    public function logout() {
        // セッションを破棄
        $this->request->session()->destroy();
        return $this->redirect($this->Auth->logout());
    }

    // 認証を使わないページの設定
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        // $this->Auth->allow(['login', 'index', 'add']); // 後でloginだけにする
        $this->Auth->allow(['login']);
    }

    // 認証時のロールのチェック
    public function isAuthorized($user = null) {
        // 管理者はtrue
        if ($user['role'] === 'admin') {
            return true;
        }
        // 一般ユーザーはAuctinControllerのみtrue,他はfalse
        if ($user['role'] === 'user') {
            if ($this->name == 'Auction') {
                return true;
            } else {
                return false;
            }
        }
        // その他は全てfalse
        return false;
    }
}